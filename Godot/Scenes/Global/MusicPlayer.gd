extends Node


const lowest_db : float = -40.0

onready var stream_player = $AudioStreamPlayer

var volume : float = 0.0 setget _set_volume
var fade_time : float = 1.0

var switching_tracks : bool = false
var next_track : AudioStream
var continuous_tracks := {}
var current_continuous_id : String = ""
var next_continuous_id : String = ""


func _process(delta : float) -> void:
	if switching_tracks:
		var volume_step : float = ((volume - lowest_db) * delta) / fade_time
		stream_player.volume_db -= volume_step
		if stream_player.volume_db <= lowest_db:
			_save_current_track_position()
			stream_player.stop()
			stream_player.stream = next_track
			_start_next_track()
			switching_tracks = false
	elif stream_player.volume_db < volume:
		var volume_step : float = ((volume - lowest_db) * delta) / fade_time
		var next_volume = stream_player.volume_db + volume_step
		stream_player.volume_db = min(next_volume, volume)


func _save_current_track_position() -> void:
	if current_continuous_id.length() > 0:
		var position : float = stream_player.get_playback_position()
		continuous_tracks[current_continuous_id] = position


func _start_next_track() -> void:
	if next_continuous_id.length() > 0:
		var seek : float = continuous_tracks[next_continuous_id]
		if seek == 0:
			stream_player.volume_db = volume
		stream_player.play(seek)
	else:
		stream_player.volume_db = volume
		stream_player.play()
	current_continuous_id = next_continuous_id


func play(audio_stream : AudioStream, continuous_id : String = "") -> void:
	if stream_player.stream != audio_stream or switching_tracks:
		switching_tracks = true
		next_track = audio_stream
		next_continuous_id = continuous_id
		if continuous_id.length() > 0 and !continuous_tracks.has(continuous_id):
			continuous_tracks[continuous_id] = 0.0


func reset_continuous_stream(continuous_id : String) -> void:
	if continuous_id.length() > 0:
		if continuous_tracks.has(continuous_id):
			continuous_tracks[continuous_id] = 0.0
		if current_continuous_id == continuous_id:
			current_continuous_id = ""


func _set_volume(new_volume : float) -> void:
	if !switching_tracks:
		volume = new_volume
		stream_player.volume_db = volume



