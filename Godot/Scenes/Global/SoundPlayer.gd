extends Node


func play(sound : AudioStream, volume_db : float = 0) -> void:
	var stream_player = _find_unused_stream_player()
	if stream_player != null:
		stream_player.stop()
		stream_player.volume_db = volume_db
		stream_player.stream = sound
		stream_player.play()


func _find_unused_stream_player() -> AudioStreamPlayer:
	for child in get_children():
		if child is AudioStreamPlayer:
			var stream_player = child as AudioStreamPlayer
			if !stream_player.playing:
				return stream_player
	return null


func play_button_sound() -> void:
	play(preload("res://Audio/button.wav"))


func play_release_sound() -> void:
	play(preload("res://Audio/release.wav"))


func play_splash_sound() -> void:
	play(preload("res://Audio/splash.wav"), -9.0)


func play_signal_sound() -> void:
	play(preload("res://Audio/signal.wav"), -6.0)


func play_static_sound() -> void:
	play(preload("res://Audio/static.wav"), -6.0)


