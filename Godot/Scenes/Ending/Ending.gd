extends Node2D


onready var ui = $DownscapeUI
onready var depth_meter = $DownscapeUI/DepthMeter
onready var energy_bar = $DownscapeUI/EnergyBar
onready var ending_timer = $EndingTimer
onready var darkening = $Darkening

var mc_face = preload("res://Sprites/mission_control.png")


func _ready():
	MusicPlayer.play(preload("res://Audio/UnchartedDawn.ogg"))
	depth_meter.update_depth(118542)
	energy_bar.update_value(0)
	ui.queue_transmission(mc_face, "We're getting a signal again. Your readings are low...")
	ui.queue_transmission(mc_face, "...is this feed live? Where the hell are you?")


func _on_SceneTimer_timeout():
	darkening.z_index = 1
	darkening.going_dark = true
	ending_timer.start()


func _on_EndingTimer_timeout():
	get_tree().change_scene("res://Scenes/Downscape/Downscape.tscn")

