extends Node2D


onready var darkening = $Darkening
onready var end_timer = $EndTimer


func _ready() -> void:
	MusicPlayer.play(preload("res://Audio/UnchartedDawn.ogg"))


func _on_SceneTimer_timeout():
	darkening.going_dark = true
	end_timer.start()


func _on_EndTimer_timeout():
	get_tree().change_scene("res://Scenes/Downscape/Downscape.tscn")


