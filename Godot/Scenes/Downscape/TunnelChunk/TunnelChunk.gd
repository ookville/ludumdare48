extends Node2D

class_name TunnelChunk


signal diver_entered

onready var left_graphic_poly = $LeftWall/Polygon2D
onready var left_collision_poly = $LeftWall/CollisionPolygon2D
onready var right_graphic_poly = $RightWall/Polygon2D
onready var right_collision_poly = $RightWall/CollisionPolygon2D

var chunk_height : float = 640
var chunk_width : float = 640 setget _set_chunk_width
var chunk_half : float = chunk_width / 2
var chunk_step : float = 16
var tunnel_width : float
var tunnel_half : float
var wall_width : float
var wall_half : float

var level : int = 0
var triggered : bool = false

var center_points := []
var left_surface := []
var right_surface := []


func _set_chunk_width(new_width : float) -> void:
	chunk_width = new_width
	chunk_half = new_width / 2


func generate_chunk(width : float,
center_noise : OpenSimplexNoise, 
left_noise : OpenSimplexNoise, 
right_noise : OpenSimplexNoise) -> void:
	wall_width = 64
	wall_half = wall_width / 2
	tunnel_width = width - (wall_width * 2)
	tunnel_half = tunnel_width / 2
	_map_tunnel_center(center_noise)
	_build_left_wall(left_noise)
	_build_right_wall(right_noise)


func _map_tunnel_center(center_noise : OpenSimplexNoise) -> void:
	center_points.clear()
	var y : float = 0
	while true:
		var t : float = position.y + y
		var x = chunk_half + (chunk_half * center_noise.get_noise_1d(t))
		center_points.append(Vector2(x, y))
		if y < chunk_height:
			y = clamp(y+chunk_step, 0, chunk_height)
		else:
			break


func _build_left_wall(wall_noise : OpenSimplexNoise) -> void:
	var vertices := []
	left_surface.clear()
	vertices.append(Vector2(-1000, chunk_height))
	vertices.append(Vector2(-1000, 0))
	for center_point in center_points:
		var wall_offset = wall_half * wall_noise.get_noise_1d(position.y + center_point.y)
		var wall_x = center_point.x - tunnel_half + wall_offset
		var point := Vector2(wall_x, center_point.y)
		vertices.append(point)
		left_surface.append(point)
	_set_left_vertices(vertices)


func _build_right_wall(wall_noise : OpenSimplexNoise) -> void:
	var vertices := []
	right_surface.clear()
	vertices.append(Vector2(chunk_width+1000, chunk_height))
	vertices.append(Vector2(chunk_width+1000, 0))
	for center_point in center_points:
		var wall_offset = wall_half * wall_noise.get_noise_1d(position.y + center_point.y)
		var wall_x = center_point.x + tunnel_half + wall_offset
		var point := Vector2(wall_x, center_point.y)
		vertices.append(point)
		right_surface.append(point)
	_set_right_vertices(vertices)


func _get_next_x(center_noise_value : float, wall_noise_value : float, dir : float) -> float:
	var center_x = chunk_half + (chunk_half * center_noise_value)
	var wall_offset = wall_half * wall_noise_value
	return center_x + (dir * tunnel_half) + wall_offset


func _set_left_vertices(vertices : Array) -> void:
	left_graphic_poly.polygon = PoolVector2Array(vertices)
	left_collision_poly.polygon = PoolVector2Array(vertices)


func _set_right_vertices(vertices : Array) -> void:
	right_graphic_poly.polygon = PoolVector2Array(vertices)
	right_collision_poly.polygon = PoolVector2Array(vertices)


func get_chunk_bottom() -> float:
	return position.y + chunk_height


func get_random_point() -> Vector2:
	var i = randi() % left_surface.size()
	var left_point : Vector2 = left_surface[i]
	var right_point : Vector2 = right_surface[i]
	var point = position + Vector2(rand_range(left_point.x+32, right_point.x-32), left_point.y)
	return point


func get_random_wall_point() -> Vector2:
	var coin : int = randi() % 2
	var i : int = randi() % left_surface.size()
	if coin == 0:
		return position + left_surface[i] + Vector2(32, 0)
	return position + right_surface[i] - Vector2(32, 0)


func _on_TriggerArea_body_entered(body):
	if !triggered and body is Diver:
		triggered = true
		emit_signal("diver_entered")

