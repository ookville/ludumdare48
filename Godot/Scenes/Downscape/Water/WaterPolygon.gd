extends Polygon2D


var polygon_points : Array
var surface_width : float = 1280
var surface_height : float = 3840
var surface_period : float = 32
var surface_point_count : int = 0

var amp : float = 4.0
var freq : float = 0.03

var t : float = 0
var dt : float = PI / 2

var noise : OpenSimplexNoise = OpenSimplexNoise.new()
var noise_amp : float = 8

var splash_noise := []
var splash_instances := []


func _ready() -> void:
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 12.0
	noise.persistence = 0.8
	polygon_points = _initialize_polygon()
	splash_noise = _initialize_splash_noise(polygon_points.size())
	_update_surface()


func _initialize_polygon() -> Array:
	var vertices := []
	var x : float = 0
	vertices.append(Vector2(0, 0))
	surface_point_count = 1
	while x < surface_width:
		x += surface_period
		vertices.append(Vector2(round(x), 0))
		surface_point_count += 1
	vertices.append(Vector2(x, surface_height))
	vertices.append(Vector2(0, surface_height))
	return vertices


func _initialize_splash_noise(points : int) -> Array:
	var array := []
	for i in points:
		array.append(0)
	return array


func _process(delta : float) -> void:
	t += dt * delta
	_update_splash_noise(delta)
	_update_surface()


func _update_surface() -> void:
	var surface_base : float = -amp * 2
	for i in surface_point_count:
		var point : Vector2 = polygon_points[i]
		var y = surface_base + (amp * sin(point.x*freq + t))
		y -= splash_noise[i]
		y += noise_amp * noise.get_noise_1d(i-t)
		polygon_points[i].y = round(y)
		polygon = PoolVector2Array(polygon_points)


func add_splash(x : float, magnitude : float) -> void:
	var effect = preload("res://Scenes/Downscape/Water/SplashEffect.tscn").instance()
	effect.position.x = x
	add_child(effect)
	var i = round(x / surface_period)
	_add_splash_point(i, magnitude)


func _add_splash_point(i : float, magnitude : float) -> void:
	if i >= 0 and i < splash_noise.size():
		var splash : Splash = Splash.new()
		splash.i = i
		splash.base_magnitude = magnitude
		splash_instances.append(splash)


func _update_splash_noise(delta : float) -> void:
	for i in splash_noise.size():
		splash_noise[i] = 0
	var dead_splashes := []
	for instance in splash_instances:
		var splash : Splash = instance
		splash.age += delta
		if splash.current_magnitude < 1:
			dead_splashes.append(splash)
		else:
			splash.add_splash_noise(splash_noise)
	for splash in dead_splashes:
		splash_instances.erase(splash)


class Splash:
	var i : int = 0
	var dx : float = 32
	var base_magnitude : float = 64
	var current_magnitude : float = 64
	var frequency : float = 10
	var time_decay : float = 0.90
	var distance_decay : float = 0.999
	var age : float = 0 setget _set_age
	var offset : float = 0
	
	func _init() -> void:
		offset = randf() * TAU
	
	func _set_age(new_age : float) -> void:
		age = new_age
		current_magnitude = base_magnitude * pow(1-time_decay, age)
	
	func add_splash_noise(noise_array : Array) -> void:
		noise_array[i] += _get_splash_value(current_magnitude, 0, 1)
		_ripple(noise_array, 1, current_magnitude, 1)
		_ripple(noise_array, -1, current_magnitude, -1)
	
	func _ripple(noise_array : Array, di : int, amp : float, direction : int) -> void:
		var point = i + di
		if point >= 0 and point < noise_array.size():
			var ripple_amp : float = current_magnitude * pow(1-distance_decay, abs(di)/dx)
			if ripple_amp >= 1:
				noise_array[point] += _get_splash_value(ripple_amp, di, direction)
				_ripple(noise_array, di + direction, ripple_amp, direction)
	
	func _get_splash_value(amp : float, point : int, direction : int) -> float:
		return amp * cos(frequency*(age+offset)*direction + point*dx)


