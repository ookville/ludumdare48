extends Node


var zones := {}
var unlocked_normal_npc_list := []
var unlocked_wall_npc_list := []


func _ready() -> void:
	var shallow_zone := Zone.new()
	shallow_zone.chunk_level = 0
	shallow_zone.modulate = Color(0.5, 0.5, 0.5)
	zones[shallow_zone.chunk_level] = shallow_zone
	
	var light_zone_1 := Zone.new()
	light_zone_1.chunk_level = 4
	light_zone_1.modulate = Color(0.35, 0.35, 0.35)
	light_zone_1.npc_density = 2
	light_zone_1.music = preload("res://Audio/Undertow.ogg")
	light_zone_1.npc_unlocks.append(preload("res://Scenes/Downscape/NPC/Jellysquash/Jellysquash.tscn"))
	light_zone_1.wall_unlocks.append(preload("res://Scenes/Downscape/NPC/MedusaGrass/MedusaGrass.tscn"))
	zones[light_zone_1.chunk_level] = light_zone_1
	
	var light_zone_2 := Zone.new()
	light_zone_2.chunk_level = 6
	light_zone_2.modulate = Color(0.25, 0.25, 0.25)
	light_zone_2.tunnel_width = 550
	light_zone_2.npc_density = 3
	light_zone_2.npc_unlocks.append(preload("res://Scenes/Downscape/NPC/Jellysquash/Demonsquash.tscn"))
	light_zone_2.npc_unlocks.append(preload("res://Scenes/Downscape/NPC/Dartfish/Dartfish.tscn"))
	zones[light_zone_2.chunk_level] = light_zone_2
	
	var dim_zone := Zone.new()
	dim_zone.chunk_level = 10
	dim_zone.modulate = Color(0.10, 0.10, 0.10)
	dim_zone.tunnel_width = 450
	dim_zone.npc_density = 4
	dim_zone.npc_unlocks.append(preload("res://Scenes/Downscape/NPC/Dartfish/Demonfish.tscn"))
	#Plankton
	zones[dim_zone.chunk_level] = dim_zone
	
	var magma_zone := Zone.new()
	magma_zone.chunk_level = 16
	magma_zone.modulate = Color(0.25, 0.10, 0.10)
	magma_zone.tunnel_width = 400
	magma_zone.npc_density = 5
	magma_zone.blurb = "Geological activity detected. This confirms many theories! However..."
	magma_zone.wall_unlocks.append(preload("res://Scenes/Downscape/NPC/MedusaGrass/MedusaNettle.tscn"))
	#Red Plankton
	zones[magma_zone.chunk_level] = magma_zone
	
	var magma_zone_2 := Zone.new()
	magma_zone_2.chunk_level = 18
	magma_zone_2.modulate = Color(0.35, 0.10, 0.10)
	magma_zone_2.tunnel_width = 400
	magma_zone_2.npc_density = 5
	magma_zone_2.blurb = "We're [???] signal. Your Oxy- [???] dangerous levels [???]"
	magma_zone_2.music = preload("res://Audio/AbyssalTide.ogg")
	zones[magma_zone_2.chunk_level] = magma_zone_2
	
	var abyss := Zone.new()
	abyss.chunk_level = 22
	abyss.modulate = Color(0, 0, 0)
	abyss.tunnel_width = 300
	abyss.npc_density = 5
	abyss.blurb = "[???] [interference]"
	abyss.wall_unlocks.append(preload("res://Scenes/Downscape/NPC/Jagger/Jagger.tscn"))
	#Jellywraith
	zones[abyss.chunk_level] = abyss
	
	var portal := Zone.new()
	portal.chunk_level = 26
	portal.modulate = Color(0, 0.1, 0)
	portal.tunnel_width = 500
	portal.npc_density = 8
	portal.blurb = "[???] fast downward current! You need to [???] [signal lost]"
	portal.fast_current = true
	zones[portal.chunk_level] = portal


func unlock_chunk(chunk_level : int) -> void:
	if zones.has(chunk_level):
		var zone : Zone = zones[chunk_level]
		for unlock in zone.npc_unlocks:
			if !unlocked_normal_npc_list.has(unlock):
				unlocked_normal_npc_list.append(unlock)
		for unlock in zone.wall_unlocks:
			if !unlocked_wall_npc_list.has(unlock):
				unlocked_wall_npc_list.append(unlock)


func get_chunk_modulate(chunk_level : int) -> Color:
	var i = chunk_level
	while i >= 0:
		if zones.has(i):
			var zone : Zone = zones[i]
			return zone.modulate
		i -= 1
	return Color(0.50, 0.50, 0.50)


func get_chunk_tunnel_width(chunk_level : int) -> float:
	var i = chunk_level
	while i >= 0:
		if zones.has(i):
			var zone : Zone = zones[i]
			return zone.tunnel_width
		i -= 1
	return 600.0


func get_npc_density(chunk_level : int) -> int:
	var i = chunk_level
	while i >= 0:
		if zones.has(i):
			var zone : Zone = zones[i]
			return zone.npc_density
		i -= 1
	return 0


func is_fast_current(chunk_level : int) -> bool:
	if zones.has(chunk_level):
		var zone : Zone = zones[chunk_level]
		return zone.fast_current
	return false


func get_blurb(chunk_level : int) -> String:
	if zones.has(chunk_level):
		var zone : Zone = zones[chunk_level]
		return zone.blurb
	return ""


func get_music(chunk_level : int) -> Resource:
	if zones.has(chunk_level):
		var zone : Zone = zones[chunk_level]
		return zone.music
	return null


class Zone:
	var chunk_level : int = 0
	var modulate : Color = Color.gray
	var tunnel_width : float = 600
	var npc_density : int = 0
	var blurb : String = ""
	var music : Resource = null
	var fast_current : bool = false
	var npc_unlocks := []
	var wall_unlocks := []

