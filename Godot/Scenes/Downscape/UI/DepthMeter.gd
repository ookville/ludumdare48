extends PanelContainer


onready var label = $MarginContainer/HBoxContainer/Label


func update_depth(depth : float) -> void:
	var rounded_depth = floor(abs(depth))
	var string_depth := String(rounded_depth)
	var padding_count : int = 6 - string_depth.length()
	var padding = ""
	for i in padding_count:
		padding += "0"
	var negative_sign = ""
	if depth < 0:
		negative_sign = "-"
	var final_string = "%s%s%s m" % [negative_sign, padding, string_depth]
	label.text = final_string


