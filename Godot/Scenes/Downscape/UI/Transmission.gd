extends PanelContainer


onready var texture_rect = $MarginContainer/HBoxContainer/TextureRect
onready var label = $MarginContainer/HBoxContainer/Label
onready var visibility_timer = $VisibilityTimer

var showing_transmission : bool = false
var transmission_queue := []


func queue_transmission(image : Texture, text : String) -> void:
	var item := TransmissionItem.new()
	item.image = image
	item.text = text
	if showing_transmission:
		transmission_queue.append(item)
	else:
		_show_transmission(item)


func _show_transmission(item : TransmissionItem) -> void:
	showing_transmission = true
	visible = true
	texture_rect.texture = item.image
	label.visible_characters = 0
	label.text = item.text
	SoundPlayer.play_signal_sound()
	if label.text.find("[") >= 0:
		SoundPlayer.play_static_sound()


func _on_CharacterTimer_timeout():
	if visible and label.visible_characters < label.text.length():
		label.visible_characters += 1
		if label.visible_characters == label.text.length():
			visibility_timer.start()


func _on_VisibilityTimer_timeout():
	if transmission_queue.size() > 0:
		_show_transmission(transmission_queue.pop_front())
	else:
		showing_transmission = false
		visible = false


class TransmissionItem:
	var image : Texture
	var text : String


