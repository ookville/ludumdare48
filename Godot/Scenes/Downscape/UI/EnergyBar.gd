extends VBoxContainer


onready var bar = $ProgressBar


func update_value(new_value : float) -> void:
	bar.value = new_value


func update_max_value(new_value : float) -> void:
	bar.max_value = new_value


