extends Control


onready var transmission = $Transmission
onready var energy_bar = $EnergyBar
onready var depth_meter = $DepthMeter

export var diver_node : NodePath

var diver : Diver = null


func queue_transmission(image : Texture, text : String) -> void:
	transmission.queue_transmission(image, text)


func _ready() -> void:
	diver = get_node_or_null(diver_node)
	if diver != null:
		energy_bar.update_max_value(diver.max_energy)
		energy_bar.update_value(diver.energy)
		diver.connect("energy_updated", self, "_on_diver_energy_updated")


func _on_diver_energy_updated() -> void:
	energy_bar.update_value(diver.energy)


func _on_DepthUpdateTimer_timeout():
	if diver != null:
		depth_meter.update_depth(diver.position.y / 16)

