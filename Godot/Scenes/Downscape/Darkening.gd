extends Polygon2D


var darkening_speed : float = 0.25
var going_dark : bool = false


func _process(delta : float) -> void:
	if going_dark and modulate.a < 1:
		var da : float = darkening_speed * delta
		modulate.a = clamp(modulate.a + da, 0, 1)
	elif !going_dark and modulate.a > 0:
		var da : float = darkening_speed * delta
		modulate.a = clamp(modulate.a - da, 0, 1)


