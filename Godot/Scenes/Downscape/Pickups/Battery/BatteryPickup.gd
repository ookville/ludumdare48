extends "res://Scenes/Downscape/Pickups/Pickup.gd"


export var energy : float = 20


func _ready() -> void:
	$AnimationPlayer.play("float")


func _activate_pickup(diver : Diver) -> void:
	diver.energy += energy

