extends Area2D

class_name Pickup


func _activate_pickup(diver : Diver) -> void:
	#Override this in inherited nodes
	pass


func _on_Pickup_body_entered(body):
	if body is Diver and (body as Diver).energy > 0:
		_activate_pickup(body)
		(body as Diver).powerup_stream_player.play()
		var effect = preload("res://Scenes/Downscape/Pickups/PikcupEffect.tscn").instance()
		effect.position = position
		get_parent().add_child(effect)
		queue_free()


