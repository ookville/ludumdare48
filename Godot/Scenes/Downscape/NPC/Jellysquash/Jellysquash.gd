extends "res://Scenes/Downscape/NPC/Npc.gd"


onready var sprite = $Sprite
onready var movement_particles = $MovementParticles

var moving : bool = false
var impulse := Vector2(0, -20)


func _ready() -> void:
	rotate(rand_range(-PI/4, PI/4))


func _on_MovementTimer_timeout():
	if moving:
		moving = false
		movement_particles.emitting = false
		sprite.frame = 0
	else:
		moving = true
		movement_particles.emitting = true
		sprite.frame = 1
		apply_central_impulse(impulse.rotated(rotation))


