extends RigidBody2D

class_name Npc


signal lit_up

onready var bubble_effect = $BubbleEffect
onready var scan_particles = $ScanParticles
onready var scan_complete_effect = $ScanCompleteEffect
onready var scan_effect_timer = $ScanEffectTimer
onready var damage_area = $DamageArea
onready var damage_timer = $DamageTimer

onready var scan_stream_player = $ScanStreamPlayer

export var npc_name : String = "Unknown"
export var school_size : int = 1
export var max_hp : float = 10
export var hostile : bool = false
export var damage : float = 10
export var scan_required : float = 2.0
export(String, MULTILINE) var scan_text : String = "Just some weird shit."

var hp : float = max_hp setget _set_hp
var scan_progress : float = 0.0 setget _set_scan_progress
var damage_ready : bool = true

var lit : bool = false
var light_source : Node2D = null

var target : Diver = null


func _ready() -> void:
	hp = max_hp


func _set_hp(value : float) -> void:
	hp = clamp(value, 0, max_hp)
	if hp <= 0:
		_activate_death_effect()
		queue_free()


func _set_scan_progress(progress : float) -> void:
	if scan_progress < scan_required:
		scan_progress = progress
		if scan_progress >= scan_required:
			scan_complete_effect.emitting = true
			scan_stream_player.play()
		else:
			scan_particles.emitting = true
			if scan_effect_timer.is_stopped():
				scan_effect_timer.start()
			else:
				scan_effect_timer.stop()
				scan_effect_timer.start()


func _physics_process(delta : float) -> void:
	if scale.x == 1:
		bubble_effect.global_rotation = 0
	elif scale.x == -1:
		bubble_effect.global_rotation = PI


func _activate_death_effect() -> void:
	for child in get_children():
		if child is DeathEffect:
			child.activate()
			remove_child(child)
			get_parent().call_deferred("add_child", child)


func set_lit(source : Node2D) -> void:
	lit = true
	light_source = source
	emit_signal("lit_up")


func clear_lit() -> void:
	lit = false
	light_source = null


func _on_ScanEffectTimer_timeout():
	scan_particles.emitting = false


func _damage_diver(diver : Diver) -> void:
	damage_ready = false
	damage_timer.start()
	diver.damage_energy(damage)


func scan_npc(scan_amount : float, scan_position : Vector2) -> void:
	self.scan_progress += scan_amount


func _on_DamageTimer_timeout():
	damage_ready = true
	for body in damage_area.get_overlapping_bodies():
		if body is Diver:
			_damage_diver(body)
			break


func _on_DamageArea_body_entered(body):
	if hostile and damage_ready and body is Diver:
		_damage_diver(body)


func _on_TargetArea_body_entered(body):
	if body is Diver:
		target = body


func _on_TargetArea_body_exited(body):
	if body == target:
		target = null

