extends "res://Scenes/Downscape/NPC/Npc.gd"


onready var sprite = $Sprite

export var swim_impulse : float = 10


func _swim() -> void:
	var impulse_vector : Vector2
	if lit:
		impulse_vector = (global_position - light_source.global_position).normalized() * swim_impulse
		impulse_vector = impulse_vector.rotated(rand_range(-PI/4, PI/4))
	elif hostile and target != null:
		impulse_vector = (target.global_position - global_position).normalized() * swim_impulse
	else:
		impulse_vector = Vector2(1, 0).rotated(randf() * TAU) * swim_impulse
	if impulse_vector.x > 0:
		sprite.scale.x = 1
	elif impulse_vector.x < 0:
		sprite.scale.x = -1
	apply_central_impulse(impulse_vector)


func _on_DartTimer_timeout():
	_swim()


func _on_Dartfish_lit_up():
	_swim()

