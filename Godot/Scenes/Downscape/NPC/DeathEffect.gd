extends CPUParticles2D

class_name DeathEffect


func activate() -> void:
	emitting = true
	$KillTimer.start()


