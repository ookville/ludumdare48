extends "res://Scenes/Downscape/NPC/Npc.gd"


onready var raycast = $RayCast2D

var stuck : bool = false


func _physics_process(delta):
	if !stuck:
		_check_wallstick()


func _check_wallstick() -> void:
	raycast.force_raycast_update()
	if raycast.is_colliding():
		_stick_to_wall()
	else:
		global_rotation += PI
		raycast.force_raycast_update()
		if raycast.is_colliding():
			_stick_to_wall()


func _stick_to_wall() -> void:
	stuck = true
	global_position = raycast.get_collision_point()
	global_rotation = raycast.get_collision_normal().angle()


