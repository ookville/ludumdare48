extends "res://Scenes/Downscape/NPC/Npc.gd"


onready var segments = $Segments
onready var eye_position = $EyePosition
onready var eye_gush_effect = $EyePosition/EyeGushEffect
onready var sprite = $Sprite
onready var blind_timer = $BlindTimer
onready var raycast = $RayCast2D
onready var anti_smash_cast = $AntiSmashRaycast

enum {
	STATE_IDLE,
	STATE_ATTACKING,
	STATE_RETRACTING,
	STATE_BLIND
}

var state : int = STATE_IDLE

var noise : OpenSimplexNoise
var t : float = 0
var t_mult : float = 20
var segment_bob : float = 8

var home_position : Vector2
var eye_scan_range_squared : float = 4 * 4
var blind : bool = false
var attacking : bool = false
var attack_speed : float = 150
var max_extension : float = 180
var initialized : bool = false


func _ready() -> void:
	home_position = position
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 64
	noise.persistence = 0.5


func _process(delta : float) -> void:
	if !initialized:
		raycast.force_raycast_update()
		if raycast.is_colliding():
			initialized = true
		else:
			scale.x *= -1
			raycast.force_raycast_update()
			if raycast.is_colliding():
				initialized = true
	t += delta * t_mult
	for node in segments.get_children():
		var x : float = (node as Node2D).position.x
		node.set_sprite_y(noise.get_noise_1d(t + x) * segment_bob)


func _physics_process(delta : float) -> void:
	var move_delta = attack_speed * delta
	match state:
		STATE_IDLE:
			if target != null:
				state = STATE_ATTACKING
		STATE_ATTACKING:
			if anti_smash_cast.is_colliding():
				state = STATE_RETRACTING
			elif abs(position.x - home_position.x) < max_extension:
				position.x += move_delta * scale.x
			else:
				state = STATE_RETRACTING
		STATE_RETRACTING:
			move_delta /= 2
			if position != home_position:
				if (home_position - position).length_squared() <= move_delta * move_delta:
					position = home_position
				else:
					position.x = move_toward(position.x, home_position.x, move_delta)


func scan_npc(scan_amount : float, scan_position : Vector2) -> void:
	.scan_npc(scan_amount, scan_position)
	if !blind and \
	(scan_position - eye_position.global_position).length_squared() <= eye_scan_range_squared:
		state = STATE_BLIND
		sprite.speed_scale = 2
		eye_gush_effect.emitting = true
		blind_timer.start()


func _on_BlindTimer_timeout():
	state = STATE_RETRACTING
	sprite.speed_scale = 1
	eye_gush_effect.emitting = false


func _on_AutokillTimer_timeout():
	if !initialized:
		queue_free()


