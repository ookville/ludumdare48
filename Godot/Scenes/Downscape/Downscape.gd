extends Node2D


onready var persistence = $Persistence
onready var zone_table = $ZoneTable
onready var ambient_modulator = $CanvasModulate
onready var tunnel_chunks = $TunnelChunks
onready var diver = $Diver
onready var water_polygon = $WaterPolygon
onready var ui = $UILayer/DownscapeUI
onready var title_ui = $UILayer/TitleUi
onready var damage_aura = $UILayer/DamageAura
onready var camera = $Diver/Camera2D
onready var camera_shaker = $Diver/Camera2D/CameraShaker
onready var end_timer = $EndTimer
onready var darkening = $UILayer/Darkening

var mc_face = preload("res://Sprites/mission_control.png")
var analysis_face = preload("res://Sprites/analysis.png")

var dive_sequence_on : bool = false
var dive_sequnece_speed : float = 24.0
var dive_sequence_y_target : float = 64

var next_y : float = 0
var chunk_width : float = 640
var chunk_height : float = 640
var tunnel_width : float = 600
var chunks := []
var chunk_counter : int = 0

var final_chunk : int = 34
var win : bool = false
var alt_mode : bool = false

var damage_aura_decay : float = 0.5

var entity_list := []
var scan_list := []
var first_scan : bool = false
var rescan_warning_given : bool = false

var center_noise : OpenSimplexNoise
var left_noise : OpenSimplexNoise
var right_noise : OpenSimplexNoise


func _ready() -> void:
	randomize()
	MusicPlayer.play(preload("res://Audio/UnchartedDawn.ogg"))
	center_noise = OpenSimplexNoise.new()
	center_noise.seed = randi()
	center_noise.octaves = 8
	center_noise.period = 1024
	center_noise.persistence = 0.5
	left_noise = OpenSimplexNoise.new()
	left_noise.seed = randi()
	left_noise.octaves = 4
	left_noise.period = 32
	left_noise.persistence = 0.5
	right_noise = OpenSimplexNoise.new()
	right_noise.seed = randi()
	right_noise.octaves = 4
	right_noise.period = 32
	right_noise.persistence = 0.5
	spawn_chunk()
	spawn_chunk()
	spawn_chunk()


func start_game() -> void:
	diver.mode = RigidBody2D.MODE_CHARACTER
	SoundPlayer.play_release_sound()


func _process(delta : float) -> void:
	if dive_sequence_on:
		_dive_sequence(delta)
	var aura_alpha : float = damage_aura.modulate.a
	if aura_alpha > 0:
		var delta_alpha = damage_aura_decay * delta
		damage_aura.modulate.a = clamp(aura_alpha-delta_alpha, 0, 1)


func _physics_process(delta : float) -> void:
	var front_chunk : TunnelChunk = chunks.front()
	if front_chunk.get_chunk_bottom() < diver.position.y - chunk_height:
		water_polygon.position.y = front_chunk.position.y
		chunks.remove(0)
		front_chunk.queue_free()
		spawn_chunk()
	for entity in entity_list:
		if !is_instance_valid(entity):
			entity_list.erase(entity)
		elif (entity as Node2D).position.y < diver.position.y - chunk_height:
			entity_list.erase(entity)
			entity.queue_free()


func spawn_chunk() -> void:
	var chunk : TunnelChunk = preload("res://Scenes/Downscape/TunnelChunk/TunnelChunk.tscn").instance()
	chunk.level = chunk_counter
	chunk_counter += 1
	chunk.connect("diver_entered", self, "_on_chunk_triggered", [chunk])
	chunk.position.y = next_y
	chunk.chunk_width = chunk_width
	chunk.chunk_height = chunk_height
	chunks.append(chunk)
	tunnel_chunks.add_child(chunk)
	tunnel_width = zone_table.get_chunk_tunnel_width(chunk.level)
	chunk.generate_chunk(tunnel_width, center_noise, left_noise, right_noise)
	next_y += chunk_height
	spawn_entity_in_chunk(preload("res://Scenes/Downscape/Pickups/Battery/BatteryPickup.tscn"), chunk)
	zone_table.unlock_chunk(chunk.level)
	_spawn_chunk_npcs(chunk)


func _spawn_chunk_npcs(chunk : TunnelChunk) -> void:
	var points : int = zone_table.get_npc_density(chunk.level)
	var normal_list : Array = zone_table.unlocked_normal_npc_list
	var wall_list : Array = zone_table.unlocked_wall_npc_list
	if points == 0 or (normal_list.size() == 0 and wall_list.size() == 0):
		return
	for i in points:
		var dice : int = randi() % (normal_list.size() + wall_list.size())
		if dice >= normal_list.size():
			#Wall enemy
			var packed_npc = wall_list[dice - normal_list.size()]
			spawn_entity_on_chunk_wall(packed_npc, chunk)
		else:
			#Normal enemy
			var packed_npc = normal_list[dice]
			spawn_entity_in_chunk(packed_npc, chunk)


func spawn_entity(scene : PackedScene, spawn_position : Vector2) -> void:
	var entity = scene.instance()
	entity.position = spawn_position
	entity_list.append(entity)
	add_child(entity)
	if entity is Npc and (entity as Npc).school_size > 1:
		for i in range(1, (entity as Npc).school_size):
			var npc : Npc = scene.instance()
			npc.position = spawn_position + Vector2(8, 0).rotated(randf() * TAU)
			entity_list.append(npc)
			add_child(npc)



func spawn_entity_in_chunk(scene : PackedScene, chunk : TunnelChunk) -> void:
	var spawn_position = chunk.get_random_point()
	spawn_entity(scene, spawn_position)


func spawn_entity_on_chunk_wall(scene : PackedScene, chunk : TunnelChunk) -> void:
	var spawn_position = chunk.get_random_wall_point()
	spawn_entity(scene, spawn_position)


func _on_WaterSurface_body_entered(body):
	if body is Diver:
		(body as Diver).linear_damp = -1
		(body as Diver).controls_enabled = true
		(body as Diver).set_bubbles_enabled(true)
		water_polygon.add_splash(body.position.x - water_polygon.position.x, 50)
		SoundPlayer.play_splash_sound()


func _on_Diver_npc_scanned(npc : Npc):
	if !scan_list.has(npc.npc_name):
		scan_list.append(npc.npc_name)
		ui.queue_transmission(analysis_face, npc.scan_text)
		if !first_scan:
			first_scan = true
			ui.queue_transmission(mc_face, "We did not expect to encounter complex life here! This is the discovery of the century!")
	elif !rescan_warning_given:
		rescan_warning_given = true
		ui.queue_transmission(mc_face, "Though fun, scanning the same organism twice is unecessary.")


func _on_Diver_damaged():
	damage_aura.modulate.a = clamp(damage_aura.modulate.a + 0.25, 0, 0.75)
	camera_shaker.shake(32)


func _on_chunk_triggered(chunk : TunnelChunk) -> void:
	var color = zone_table.get_chunk_modulate(chunk.level)
	ambient_modulator.change_modulate(color)
	var blurb : String = zone_table.get_blurb(chunk.level)
	if blurb.length() > 0:
		ui.queue_transmission(mc_face, blurb)
	var chunk_music : Resource = zone_table.get_music(chunk.level)
	if chunk_music != null:
		MusicPlayer.play(chunk_music)
	if zone_table.is_fast_current(chunk.level):
		diver.gravity_scale = 2.5
	if chunk.level > final_chunk:
		win = true
		_start_end_sequence()


func _on_TitleUi_start_game():
	dive_sequence_on = true
	title_ui.visible = false
	SoundPlayer.play_button_sound()
	ui.visible = true
	ui.queue_transmission(mc_face, "Initiating research delve mission 1.")
	ui.queue_transmission(mc_face, "Make sure you keep your power topped-off with the energy nodes the drillhead left in the tunnel.")
	ui.queue_transmission(mc_face, "Your suit is self-sustaining and self-repairing, but consumes plenty of energy.")
	ui.queue_transmission(mc_face, "Analyze anything unusual with your scanning laser. This site is fascinating...")
	ui.queue_transmission(mc_face, "Be aware that moving and scanning both increase energy consumption.")


func _dive_sequence(delta : float) -> void:
	var dy : float = dive_sequnece_speed * delta
	var diff : float = abs(dive_sequence_y_target - camera.position.y)
	if diff <= dy:
		camera.position.y = dive_sequence_y_target
		dive_sequence_on = false
		start_game()
	else:
		camera.position.y += dy


func _start_end_sequence() -> void:
	if end_timer.is_stopped():
		end_timer.start()
		darkening.going_dark = true


func _on_EndTimer_timeout():
	if win:
		persistence.award_trophy()
		if alt_mode:
			get_tree().change_scene("res://Scenes/Ending/AltEnding.tscn")
		else:
			get_tree().change_scene("res://Scenes/Ending/Ending.tscn")
	else:
		get_tree().change_scene("res://Scenes/Downscape/Downscape.tscn")


func _on_Diver_out_of_energy():
	_start_end_sequence()


func _on_TitleUi_enable_hard_mode():
	alt_mode = true
	diver.passive_drain *= 2
	diver.thruster_drain *= 2
	diver.scan_drain *= 2
	diver.gravity_scale = 2.0


func _unhandled_input(event : InputEvent) -> void:
	if event is InputEventKey:
		var key_event : InputEventKey = event
		if key_event.pressed and key_event.scancode == KEY_BACKSLASH:
			persistence.award_trophy()
			title_ui.enable_alt_mode_checkbox()


