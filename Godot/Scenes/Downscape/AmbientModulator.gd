extends CanvasModulate


var old_color : Color
var target_color : Color
var progress : float = 0
var progress_speed : float = 0.20
var morphing : bool = false
var locked : bool = false


func change_modulate(new_modulate : Color) -> void:
	if !locked and new_modulate != color:
		old_color = color
		target_color = new_modulate
		progress = 0
		morphing = true


func _process(delta : float) -> void:
	if morphing:
		var progress_delta = progress_speed * delta
		progress = clamp(progress+progress_delta, 0, 1)
		color = lerp(old_color, target_color, progress)
		if progress >= 1:
			morphing = false


