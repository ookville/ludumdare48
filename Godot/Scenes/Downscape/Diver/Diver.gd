extends RigidBody2D

class_name Diver


signal energy_updated
signal out_of_energy
signal npc_scanned(npc)
signal damaged

const idle_frame : int = 0
const thrust_frame : int = 1
const transition_frame : int = 2

onready var bubble_effect = $BubbleEffect
onready var sprite = $Sprite
onready var thrust_effect = $Sprite/ThrustEffect
onready var headlight = $Sprite/Headlight
onready var scanbeam = $Sprite/Scanbeam
onready var ambient_light = $AmbientLight
onready var transition_timer = $TransitionTimer
onready var damage_effect = $DamageEffect

onready var hit_stream_player = $HitStreamPlayer
onready var engine_stream_player = $EngineStreamPlayer
onready var scan_stream_player = $ScanStreamPlayer
onready var offline_stream_player = $OfflineStreamPlayer
onready var powerup_stream_player = $PowerupStreamPlayer

var controls_enabled : bool = false
var sprite_turning : bool = false
var thrust : float = 200
var thrust_vector := Vector2.ZERO

var max_energy : float = 100
var energy : float = 100 setget _set_energy
var flicker_threshold : float = 50

var passive_drain : float = 0.5
var thruster_drain : float = 0.5
var scan_drain : float = 0.5

var t : float = 0
var t_mult : float = 20
var noise : OpenSimplexNoise


func _ready() -> void:
	scanbeam.add_exception(self)
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 8
	noise.persistence = 0.8


func _process(delta : float) -> void:
	t += t_mult * delta
	thrust_vector = Vector2.ZERO
	if controls_enabled:
		if Input.is_action_just_pressed("thrust_right") or \
		Input.is_action_just_pressed("thrust_left"):
			engine_stream_player.play()
		if Input.is_action_pressed("thrust_right"):
			thrust_vector.x += thrust
		if Input.is_action_pressed("thrust_left"):
			thrust_vector.x -= thrust
		if Input.is_action_just_pressed("activate_scanbeam"):
			scanbeam.on = true
			scan_stream_player.play()
		elif Input.is_action_just_released("activate_scanbeam"):
			scanbeam.on = false


func _physics_process(delta : float) -> void:
	applied_force = thrust_vector
	if controls_enabled:
		_update_sprite()
		_update_headlight()
		_utility_energy_drain(delta)


func _set_energy(new_value : float) -> void:
	energy = clamp(new_value, 0, max_energy)
	if energy <= 0:
		if controls_enabled:
			offline_stream_player.play()
		headlight.enabled = false
		ambient_light.enabled = false
		controls_enabled = false
		emit_signal("out_of_energy")
	emit_signal("energy_updated")


func _update_sprite() -> void:
	if !sprite_turning:
		if sprite.scale.x > 0 and get_global_mouse_position().x < position.x:
			_start_turn_animation()
		elif sprite.scale.x < 0 and get_global_mouse_position().x > position.x:
			_start_turn_animation()
		elif thrust_vector.x != 0:
			sprite.frame = thrust_frame
		else:
			sprite.frame = idle_frame
	thrust_effect.emitting = (thrust_vector.x != 0)


func _update_headlight() -> void:
	headlight.look_at(get_global_mouse_position())
	scanbeam.look_at(get_global_mouse_position())
	headlight.enabled = true
	if energy == 0:
		headlight.enabled = false
	elif energy < flicker_threshold:
		var offset = (energy / flicker_threshold) * 0.50
		if (noise.get_noise_1d(t) + offset) <= 0:
			headlight.enabled = false


func _utility_energy_drain(delta) -> void:
	if thrust_vector.x != 0:
		self.energy -= thruster_drain * delta
	if scanbeam.on:
		self.energy -= scan_drain * delta


func _start_turn_animation() -> void:
	sprite.frame = transition_frame
	sprite.scale.x = -sprite.scale.x
	transition_timer.start()
	sprite_turning = true


func damage_energy(amount : float) -> void:
	self.energy -= amount
	damage_effect.emitting = true
	hit_stream_player.play()
	emit_signal("damaged")


func set_bubbles_enabled(is_enabled : bool) -> void:
	bubble_effect.emitting = is_enabled


func _on_TransitionTimer_timeout():
	sprite_turning = false
	_update_sprite()
	_update_headlight()


func _on_EnergyDrainTimer_timeout():
	if controls_enabled:
		self.energy -= passive_drain


func _on_Scanbeam_scan_complete(npc):
	emit_signal("npc_scanned", npc)


