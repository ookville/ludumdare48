extends Area2D


func _on_LightArea_body_entered(body):
	if body is Npc:
		(body as Npc).set_lit(self)


func _on_LightArea_body_exited(body):
	if body is Npc:
		(body as Npc).clear_lit()


