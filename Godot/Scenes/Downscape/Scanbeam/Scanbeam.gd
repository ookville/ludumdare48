extends RayCast2D


signal scan_complete(npc)

onready var beam_line = $BeamLine
onready var beam_end = $BeamEnd
onready var beam_particles = $BeamParticles
onready var animation_player = $AnimationPlayer

var scan_power : float = 1.0
var on : bool = false setget _set_on


func _ready() -> void:
	self.on = false
	animation_player.play("pulse")


func _set_on(is_on : bool) -> void:
	on = is_on
	beam_line.visible = is_on
	beam_end.visible = is_on
	beam_particles.emitting = is_on


func _physics_process(delta : float) -> void:
	_update_beam()
	if on:
		_process_beam_target(scan_power * delta)


func _update_beam() -> void:
	if is_colliding():
		var collision_point : Vector2 = get_collision_point()
		beam_line.points[1] = to_local(collision_point)
		beam_end.global_position = collision_point
		_update_beam_particles()
	else:
		beam_line.points[1] = cast_to
		beam_end.position = cast_to
		_update_beam_particles()


func _update_beam_particles() -> void:
	beam_particles.position = beam_line.points[1] / 2
	beam_particles.emission_rect_extents.x = beam_line.points[1].x / 2

func _process_beam_target(scan_power) -> void:
	if is_colliding():
		var collider = get_collider()
		if collider is Npc:
			var npc : Npc = collider
			if npc.scan_progress < npc.scan_required:
				npc.scan_npc(scan_power, get_collision_point())
				if npc.scan_progress >= npc.scan_required:
					emit_signal("scan_complete", npc)
			else:
				npc.scan_npc(scan_power, get_collision_point())


