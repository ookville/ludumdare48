extends Control


signal start_game
signal enable_hard_mode


onready var persistence = $Persistence
onready var alt_mode_container = $AltModeContainer
onready var alt_mode_checkbox = $AltModeContainer/AltMode/CheckBox


func _ready() -> void:
	if persistence.check_trophy():
		alt_mode_container.visible = true
	else:
		alt_mode_container.visible = false


func enable_alt_mode_checkbox() -> void:
	alt_mode_container.visible = true


func _on_LaunchButton_pressed():
	if alt_mode_checkbox.pressed:
		emit_signal("enable_hard_mode")
	emit_signal("start_game")


