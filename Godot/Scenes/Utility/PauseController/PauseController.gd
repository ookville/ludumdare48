extends Node


func _unhandled_input(event : InputEvent) -> void:
	if event is InputEventKey:
		var key_event : InputEventKey = event
		if key_event.pressed and key_event.scancode == KEY_ESCAPE:
			get_tree().paused = !get_tree().paused
			get_tree().set_input_as_handled()

