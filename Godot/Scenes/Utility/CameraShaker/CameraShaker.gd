extends Node2D


var noise :OpenSimplexNoise

var t : float = 0
var t_accel : float = 10
var magnitude : float = 0
var damping : float = 4.0


func _ready() -> void:
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 8
	noise.persistence = 0.8


func _process(delta : float) -> void:
	if magnitude > 0:
		t += delta * t_accel
		magnitude = clamp(magnitude - (magnitude*damping*delta), 0, 100000)
		(get_parent() as Camera2D).offset.x = noise.get_noise_2d(t, 0) * magnitude
		(get_parent() as Camera2D).offset.y = noise.get_noise_2d(0, t) * magnitude


func shake(impulse : float) -> void:
	magnitude = max(magnitude, impulse)


