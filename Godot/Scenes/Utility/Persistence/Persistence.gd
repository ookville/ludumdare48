extends Node


const trophy_file_name : String = "user://trophy.txt"


func check_trophy() -> bool:
	var file : File = File.new()
	if file.file_exists(trophy_file_name):
		return true
	return false


func award_trophy() -> void:
	var file : File = File.new()
	if !file.file_exists(trophy_file_name):
		file.open(trophy_file_name, File.WRITE)
		file.store_string("Hard mode and alternate ending unlocked!")
		file.close()


